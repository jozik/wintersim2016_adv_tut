#!/bin/bash
export R_HOME=/usr/lib/R
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/etc/R:/home/nick/R/x86_64-pc-linux-gnu-library/3.3
#The simulator to be used (located in tproot/scripts):
 #1. REPAST -- repast.sh
  #2. MASON -- mason.sh
   #3. NETLOGO -- netlogo.sh
    export MODEL=2
swift-t -p -I .. R_utils_tests.swift


