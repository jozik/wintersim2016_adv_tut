# run using py.test
import deap_ga
import wapq
import random

def test_create_fitnesses():
    str1 = "1,2,3;4,5,6;7,8,9"
    expected = [(0,),(1,),(2,)]
    assert expected == deap_ga.create_fitnesses(str1)

def test_run(monkeypatch):
    test_run.data_size = 0
    def stubput(data):
        test_run.data_size = len(data.split(";")) if data else 0
        print("Putting data: {}".format(data))
        print("test_run.data_size: {}".format(test_run.data_size))
    def stubget():
        stubget.counter += 1
        if stubget.counter == 1:
            # Return test deap parameters
            return '5,3,0,"../../data/params_for_deap.csv"'
        else:
            # Return artificial fitnesses of the correct size
            result = ",".join(["{}".format(random.randrange(10)) for x in range(test_run.data_size)])
            print("Getting result: ", result)
            return result
    stubget.counter = 0
    monkeypatch.setattr(wapq, 'OUT_put', stubput)
    monkeypatch.setattr(wapq, 'IN_get', stubget)
    # Assure that the deap_ga.run function runs without error
    # use "py.test -s test_deap_ga.py" to see print outputs
    deap_ga.run()
