#!/bin/bash

set -eu

# Args
param_line=$1   # The upf param line
#echo "$param_line"
instanceDir=$2  # The instance directory
tproot=$3


if [[ ${JAVA:-0} == 0 ]] 
then
  JAVA=java 
fi

echo "INPUT" $param_line

$JAVA -Xmx1536m -XX:-UseLargePages \
-jar $tproot/mason_model/mason-1.0-wrapper.jar \
-m $tproot/mason_model/JZombieMason.jar \
-simstate it.isislab.swiftlang.abm.mason.zombie.JZombie \
-outfile $tproot/swift/$instanceDir/counts.csv \
-runid 1 \
-s 150 \
-trial 1 \
-i $param_line \
-o human_count,zombie_count


