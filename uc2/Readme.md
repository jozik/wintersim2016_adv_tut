# Note: An updated Extreme-scale Model Exploration with Swift (EMEWS) tutorial website with up-to-date use cases is available here: [http://www.mcs.anl.gov/~emews/tutorial](http://www.mcs.anl.gov/~emews/tutorial)
# The direct link to the updated EMEWS UC2 tutorial is available here: [http://www.mcs.anl.gov/~emews/tutorial/?action=tutorial-view&tutorial=uc2](http://www.mcs.anl.gov/~emews/tutorial/?action=tutorial-view&tutorial=uc2)
## While this repository is being left as-is for archiving purposes, it is highly recommended that the updated tutorial site be used for information on EMEWS.

----
# Demonstration ABM Workflow - UC2  Workflow control with Python-based external algorithms



##How to include simulators with Zombies simulation in the UC2 example

###Clone the tutorial repository

- Clone using git or download the source code: ```git clone https://bitbucket.org/jozik/wintersim2016_adv_tut.git```

- Enter in the main folder: ```cd wintersim2016_adv_tut```

###Include Repast Zombies model

Download the model and extract in the project foler:

- ```wget https://bitbucket.org/jozik/wintersim2016_adv_tut/raw/dc3a17fad000dce60b2aa14c99f697df365e3343/jzombies/complete_model.jar```
- ```unzip complete_model.jar -d complete_model/```


### Include MASON and NetLogo Zombies model

#####Download and install the executor wrapper for MASON and Netlogo, with the fallowing steps:

-  ``` wget https://github.com/isislab-unisa/swiftlangabm/archive/master.zip```
-  ```unzip master.zip```

- ```(cd swiftlangabm-master/mason ; mvn assembly:assembly)```
- ```(cd swiftlangabm-master/netlogo ; mvn assembly:assembly)```

- ```mkdir uc2/swift_proj/mason_model```
- ```mkdir uc2/swift_proj/netlogo_model```

- ```cp swiftlangabm-master/mason/target/mason-1.0-wrapper.jar uc2/swift_proj/mason_model/```
- ```cp swiftlangabm-master/mason/resources/models/JZombieMason.jar uc2/swift_proj/mason_model/```

- ```cp swiftlangabm-master/netlogo/target/netlogo-1.0-wrapper.jar uc2/swift_proj/netlogo_model/```
- ```cp swiftlangabm-master/netlogo/resources/models/JZombiesLogo.nlogo uc2/swift_proj/netlogo_model/```

- ```cp uc2/swift_proj/data/parameters_for_mason_netlogo.csv uc2/swift_proj/netlogo_model/```

- ```cp uc2/swift_proj/data/parameters_for_mason_netlogo.csv uc2/swift_proj/mason_model/```

###Run the expirement example UC2

To change the simulator edit the ```wintersim2016_adv_tut/uc2/swift_proj/swift/st_local_deapex.sh``` and modify the simulator: 

```
...
#The simulator to be used (located in tproot/scripts):
#1. REPAST -- repast.sh
#2. MASON -- mason.sh
#3. NETLOGO -- netlogo.sh
export MODEL=3
...
```

###Easy install

Download and run the script test_install.sh.

```
wget https://bitbucket.org/jozik/wintersim2016_adv_tut/raw/13bb8bee52a031ef9548da2e40dc6704109c1600/uc2/test_install.sh

source test_install.sh
```
##TODO
- Positions files in MASON and NetLogo, to generate heat maps.
- Random seed setting Netlogo and MASON.




