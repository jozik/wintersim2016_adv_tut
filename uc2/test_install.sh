git clone -b multisim https://bitbucket.org/jozik/wintersim2016_adv_tut.git

cd wintersim2016_adv_tut
wget https://bitbucket.org/jozik/wintersim2016_adv_tut/raw/dc3a17fad000dce60b2aa14c99f697df365e3343/jzombies/complete_model.jar
unzip complete_model.jar -d uc2/swift_proj/complete_model


wget https://github.com/isislab-unisa/swiftlangabm/archive/master.zip

unzip master.zip

(cd swiftlangabm-master/mason ; mvn assembly:assembly)
(cd swiftlangabm-master/netlogo ; mvn assembly:assembly)

mkdir uc2/swift_proj/mason_model
mkdir uc2/swift_proj/netlogo_model

cp swiftlangabm-master/mason/target/mason-1.0-wrapper.jar uc2/swift_proj/mason_model/
cp swiftlangabm-master/netlogo/target/netlogo-1.0-wrapper.jar uc2/swift_proj/netlogo_model/

cp swiftlangabm-master/mason/resources/models/JZombieMason.jar uc2/swift_proj/mason_model/
cp swiftlangabm-master/netlogo/resources/models/JZombiesLogo.nlogo uc2/swift_proj/netlogo_model/

cp uc2/swift_proj/data/parameters_for_mason_netlogo.csv uc2/swift_proj/netlogo_model/
cp uc2/swift_proj/data/parameters_for_mason_netlogo.csv uc2/swift_proj/mason_model/

(cd ./uc2/swift_proj/swift/; ./st_local_deapex.sh)