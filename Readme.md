# Note: An updated Extreme-scale Model Exploration with Swift (EMEWS) tutorial website with up-to-date use cases is available here: [http://www.mcs.anl.gov/~emews/tutorial](http://www.mcs.anl.gov/~emews/tutorial)
## While this repository is being left as-is for archiving purposes, it is recommended that the updated tutorial site be used for information on EMEWS.


----


# Winter Simulation Conference 2016 - Advanced Tutorial
## FROM DESKTOP TO LARGE-SCALE MODEL EXPLORATION WITH Swift/T

This repository contains all the use cases and related materials for the WSC2016 Advanced Tutorial "FROM DESKTOP TO LARGE-SCALE MODEL EXPLORATION WITH Swift/T"

##### Authors

  - Jonathan Ozik (Argonne National Laboratory)
  - Nicholson Collier (Argonne National Laboratory)
  - Justin Wozniak (Argonne National Laboratory)
  - Carmine Spagnuolo (Univeristà degli Studi di Salerno)


###List of Use Cases:
  - UC1: Simple Workflows with ABM
  - UC2: Workflow control with Python-based external algorithms
  - UC3: Calling a distributed MPI-based Model


##### License