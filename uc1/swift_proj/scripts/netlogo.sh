#!/bin/bash

set -eu

# Args
param_line=$1   # The upf param line
#echo "$param_line"
instanceDir=$2  # The instance directory
tproot=$3


if [[ ${JAVA:-0} == 0 ]] 
then
  JAVA=java 
fi

echo "INPUT" $param_line

$JAVA -Xmx1536m -XX:-UseLargePages \
-jar $tproot/netlogo_model/netlogo-1.0-wrapper.jar \
-m $tproot/netlogo_model/JZombiesLogo.nlogo \
-outfile $tproot/swift/$instanceDir/counts.csv \
-runid 1 \
-s 150 \
-trial 1 \
-i $param_line \
-o human_count,zombie_count
