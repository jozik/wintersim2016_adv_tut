# swiftrunex run: N sims
export T_PROJECT_ROOT=$PWD/..
export TURBINE_OUTPUT=$T_PROJECT_ROOT/exp_dirs/exp_swiftrunex
# QUEUE, WALLTIME, PROCS, PPN, AND TURNBINE_JOBNAME will
# be ignored if the -m scheduler flag is not used
#export QUEUE=batch
#export WALLTIME=00:10:00
#export PROCS=8   # number of processes
#export PPN=1
#export TURBINE_JOBNAME="swiftrunex"
# if R cannot be found, then these will need to be
# uncommented and set correctly.
export R_HOME=/usr/lib/R
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/R/lib

#The simulator to be used (located in tproot/scripts):
#1. REPAST -- repast.sh
#2. MASON -- mason.sh
#3. NETLOGO -- netlogo.sh
export MODEL=3

# Two scripts can be run:
# 1. swiftrun.script -- plain vanilla sweep without the R analysis code
# 2. swiftrun_R.script -- sweep with R analysis 

# pbs scheduler run.
#swift-t -m pbs -p swiftrun.swift -f="$T_PROJECT_ROOT/complete_model/unrolledParamFile.txt"
# Run immediately without a scheduler. Use swiftrun.swift for the vanilla parameters sweep
# without the R analysis code.
echo "Model selected:"$MODEL 
if [ $MODEL == "1" ]; then
	swift-t -n 4 -p swiftrun_R.swift -f="$T_PROJECT_ROOT/complete_model/unrolledParamFile.txt"
else 
	swift-t -n 4 -p swiftrun_R.swift -f="$T_PROJECT_ROOT/mason_model/paramFileMasonNetlogo.txt"
fi
