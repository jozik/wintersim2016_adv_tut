import io;
import sys;
import files;
import string;
import R;

string count_humans = ----
 last.row <- tail(read.csv("%s/counts.csv"), 1)
 res <- last.row['human_count']
----;

string find_max =  ----
v <- c(%s)
res <- which(v == max(v))
----;

string make_heatmap = ----
library(plyr)
library(ggplot2)
locs <- read.csv("%s/location_output.csv")
xy <- count(locs, c("X", "Y"))
plot <- ggplot(xy, aes(X,Y)) + geom_raster(aes(fill = freq))
ggsave("%s/heat_map_%d.png", plot, width = 4, height = 4)
----;

string tproot = getenv("T_PROJECT_ROOT");
int model = toint(getenv("MODEL"));

app (file out, file err) runmodel (file shfile, string param_line, string outputdir)
{
    "bash" shfile param_line outputdir tproot @stdout=out @stderr=err;
}

app (void o) make_dir(string dirname) {
  "mkdir" "-p" dirname;
}

app (void o) cp_message_center() {
  "cp" (strcat(tproot,"/complete_model/MessageCenter.log4j.properties")) ".";
}

testresult = R("","toString(.libPaths())");


cp_message_center() => {
  file model_sh;
  string out_dir;
  switch (model)
  {
    case 1:
       model_sh = input(tproot+"/scripts/repast.sh");
       out_dir = tproot +  "/output";
    case 2:
       model_sh = input(tproot+"/scripts/mason.sh");
       out_dir = tproot +  "/output-mason";
    case 3:
       model_sh = input(tproot+"/scripts/netlogo.sh");
       out_dir = tproot +  "/output-netlogo";
  } 
  file upf = input(argv("f"));
  string upf_lines[] = file_lines(upf);
  
  string results[];
  
  foreach s,i in upf_lines {
    string instance = "instance_%i/" % (i+1);
    make_dir(instance) => {
      file out <instance+"out.txt">;
      file err <instance+"err.txt">;
	printf("input %s\n",s);
      (out,err) = runmodel(model_sh, s, instance) => {
        string code = count_humans % instance;
        results[i] = R(code, "toString(res)");
        if(model == 1){
		// make the heat map
       		string heatmap_code = make_heatmap % (instance, out_dir, i);
        	R(heatmap_code, "toString(0)");
 	}

      }
    }
  }
  
  string results_str = string_join(results, ",");
  string code = find_max % results_str;
  string max_idxs[] = split(R(code, "toString(res)"), ",");
  string best_params[];
  foreach s, i in max_idxs {
    int idx = toint(trim(s));
    best_params[i] = upf_lines[idx - 1];
  }
  file best_out <out_dir+"/best_parameters.txt"> = 
    write(string_join(best_params, "\n"));
}
