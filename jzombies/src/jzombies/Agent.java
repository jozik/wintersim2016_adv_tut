/**
 * 
 */
package jzombies;

import repast.simphony.space.grid.Grid;

/**
 * @author nick
 *
 */
public abstract class Agent {
	
	public enum AgentType {HUMAN, ZOMBIE};
	
	protected Grid<Object> grid;
	protected double stepSize;
	
	public Agent(Grid<Object> grid, double stepSize) {
		this.grid = grid;
		this.stepSize = stepSize;
	}
	
	public int getX() {
		 return grid.getLocation(this).getX();
	}
	
	public int getY() {
		return grid.getLocation(this).getY();
	}
	
	public abstract AgentType getAgentType(); 

}
